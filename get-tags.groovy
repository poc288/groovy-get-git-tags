def CREDENTIAL_ID = 'gitlab-vorrapong-personal-token'
def SECRET = com.cloudbees.plugins.credentials.SystemCredentialsProvider.getInstance().getStore().getCredentials(com.cloudbees.plugins.credentials.domains.Domain.global()).find { it.getId().equals(CREDENTIAL_ID) }.getSecret().getPlainText()

def conn = new URL("https://gitlab.com/api/v4/projects/42180670/repository/tags").openConnection();
conn.setRequestProperty('Authorization', 'Bearer ' + SECRET)
def text = conn.getInputStream().getText()

def jsonSlurper = new groovy.json.JsonSlurper()

def object = jsonSlurper.parseText(text)

def tags = []
for(int i= 0; i < object.size(); i++) {
    tags.push(object[i].name)
}

return tags